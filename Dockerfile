FROM php:8.1-fpm

RUN mkdir /code

WORKDIR /code

# Install nodejs 18
RUN apt-get update && \
    apt-get install -y sudo curl gcc g++ make && \
    curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -  && \
    apt-get install -y nodejs && \
    npm install -g npm@latest && \
    echo "NODE Version:" && node --version && \
    echo "NPM Version:" && npm --version

RUN apt-get update && \
    apt-get install -y ruby && \
    apt-get install -y unzip zlib1g-dev libzip-dev libxml2-dev libmagickwand-dev default-mysql-client libonig-dev libssl-dev libbz2-dev && \ 
    apt-get install -y git gnupg2 libpng-dev libjpeg-dev --no-install-recommends && \
    docker-php-ext-install bcmath exif pdo_mysql pcntl gd exif zip soap && \
    pecl install && \
    docker-php-ext-enable zip && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    apt-get update && apt-get upgrade -y && apt-get install -y && \
    apt-get purge --auto-remove -y curl gnupg && \
    rm -rf /var/lib/apt/lists/*

RUN echo "memory_limit = 512M" > $PHP_INI_DIR/conf.d/php-memory-limits.ini